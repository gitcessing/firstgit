/**
* This tab contains utility drawing
* functions.
*/
void drawRandomRects()
{
   fill(#00FF00);
   strokeWeight(random(2,5));
   stroke(random(200,255));
   rect( random(width),
           random(height),
           random(20,20),
           random(30,20));  
  
}
