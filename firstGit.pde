/**
* This is my first sketch which
* will be tracked via git
*
*/ 
void setup(){
   size(800,600);
}


void draw(){
   drawRandomCircles();
   drawRandomRects();
}

void drawRandomCircles(){
   fill(#FF0000);
   strokeWeight(random(2,5));
   stroke(random(200,255));
   ellipse( random(width),
           random(height),
           random(20,20),
           random(30,20));
}
